const gameField = [
  [`x`, `o`, null],
  [`x`, null, `o`],
  [`x`, `o`, `o`] 
  ];

let win = null;

let field = document.querySelector('#field');

drawField(field, 3, 3);
detour();

function drawField (parent, cols, rows) {
  table = document.createElement('table');
  for (let i = 0; i < cols; i++) {
    let tr = document.createElement('tr');
    
    for (let j = 0; j < rows; j++) {
      let td = document.createElement('td');
      td.innerHTML=gameField[i][j];
      tr.appendChild(td);
    }
    table.appendChild(tr);
  }
  parent.appendChild(table);
  }

function detour() {
 //Horizont
  if (win === null) {
    for (let i = 0;i < gameField.length;i++) {
    if (gameField[i][0] !== null && gameField[i][0] === gameField[i][1] && gameField[i][1] == gameField[i][2]) {
    win = gameField[i][0];
  }
  }
  }
//Vertical
  if (win === null) {
    for (let i = 0;i < gameField.length;i++) {
    if (gameField[0][i] !== null && gameField[0][i] === gameField[1][i] && gameField[1][i] == gameField[2][i]) {
     win = gameField[0][i]
}
}
}
//Diagonal
  if (win === null) {
    if (gameField[0][0] !== null && gameField[0][0] === gameField[1][1] && gameField[1][1] === gameField[2][2])  {
      win = gameField[0][0];
  } else if (gameField[0][2] !== null && gameField[0][2] === gameField[1][1] && gameField[1][1] === gameField[2][0]) {
   win = gameField[0][2]
}
}
}

alert(`Победил игрок ${win}`);